<HTML>
    <HEAD>
        <TITLE>Enregistrement</TITLE>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script>
		$(document).ready(function() {
			// Lorsque l'utilisateur clique sur une ville
			$('table').on('click', 'tr', function() {
				// Récupération de l'id de la ville sélectionnée
				var id = $(this).attr('id');
				// Requête AJAX pour récupérer les informations du vol correspondant
				$.ajax({
					type: 'POST',
					url: 'getvols.php',
					data: { id: id },
					success: function(data) {
						// Affichage des informations dans la colonne de droite
						$('#vol_infos').html(data);
					}
				});
			});
		});
	    </script>
        <style>
		table {
			float: left;
			margin-right: 50px;
		}
	    </style>
    </HEAD>
    <BODY>
        <H1>Enregistrement</H1>
        <?php

        $dsn = 'mysql:host=localhost;dbname=tpfinal';

        try {
            $bdd = new PDO($dsn, 'root' , '');
        }
        catch (Exception $e)
        {
            die('Erreur : ' . $e->getMessage());
        }

        $sql = "SELECT id, ville FROM vols;";
        $reponse = $bdd->query($sql) or die(printr($bdd->errorInfo()));

        echo "Liste des vols\n"

        print "<table>\n" ;
        while ($donnees = $reponse->fetch()) {
			echo '<tr id="' . $donnees['id'] . '"><td>' . $donnees['ville'] . '</td></tr>';
		}
        print "</table>\n" ;

        $reponse->closeCursor();
		$bdd = null;
        ?>

        <DIV id="vol_infos" style="float: left;"></DIV>
    </BODY>
</HTML>