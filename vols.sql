CREATE TABLE vols (
id INTEGER NOT NULL AUTO_INCREMENT,
type VARCHAR(50),
ville VARCHAR(50),
duree INTEGER,
horaire VARCHAR(10),
avion VARCHAR(10),
nb_Passagers INTEGER,
correspondance VARCHAR(10),
categorie VARCHAR(10),
PRIMARY KEY (id)
);

INSERT INTO `vols`(`id`, `type`, `ville`, `duree`, `horaire`, `avion`, `nb_Passagers`, `correspondance`, `categorie`) VALUES (NULL,'Aller', 'Lyon', 160, '09:50', 'A320', 118, 'J6498', 'Moyen');
INSERT INTO `vols`(`id`, `type`, `ville`, `duree`, `horaire`, `avion`, `nb_Passagers`, `correspondance`, `categorie`) VALUES (NULL,'Retour','Tokyo',630,'10:20','A630',204,NULL,'Long');
INSERT INTO `vols`(`id`, `type`, `ville`, `duree`, `horaire`, `avion`, `nb_Passagers`, `correspondance`, `categorie`) VALUES (NULL,'Aller', 'Bali', 535, '11:00', 'A340', 176, NULL, 'Long');
INSERT INTO `vols`(`id`, `type`, `ville`, `duree`, `horaire`, `avion`, `nb_Passagers`, `correspondance`, `categorie`) VALUES (NULL,'Aller', 'Paris', 80, '11:14', 'A320', 114, 'Y9031', 'Court');
INSERT INTO `vols`(`id`, `type`, `ville`, `duree`, `horaire`, `avion`, `nb_Passagers`, `correspondance`, `categorie`) VALUES (NULL,'Retour', 'Lisbonne', 160, '11:31', 'A340', 182, 'U6620', 'Moyen');
INSERT INTO `vols`(`id`, `type`, `ville`, `duree`, `horaire`, `avion`, `nb_Passagers`, `correspondance`, `categorie`) VALUES (NULL,'Retour', 'Tunis', 170, '12:15', 'A320', 108, NULL, 'Moyen');
INSERT INTO `vols`(`id`, `type`, `ville`, `duree`, `horaire`, `avion`, `nb_Passagers`, `correspondance`, `categorie`) VALUES (NULL,'Aller', 'New York', 420, '13:10', 'A360', 214, 'R9242', 'Long');