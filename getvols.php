<?php
	// Connexion à la base de données
	$bdd = new PDO('mysql:host=localhost;dbname=tpfinal;charset=utf8', 'root', '');

	// Récupération de l'identifiant du vol
	$id = $_POST['id'];

	// Requête SQL pour récupérer les informations du vol correspondant
	$req = $bdd->prepare('SELECT * FROM vols WHERE id = id');
	$req->execute(array($id));

	// Affichage des informations sous forme de HTML
	if ($donnees = $req->fetch()) {
		echo '<h2>Informations du vol</h2>';
		echo '<p><strong>Durée :</strong> ' . $donnees['duree'] . ' heures</p>';
		echo '<p><strong>Horaire :</strong> ' . $donnees['horaire'] . '</p>';
		echo '<p><strong>Avion :</strong> ' . $donnees['avion'] . '</p>';
		echo '<p><strong>Nombre de passagers :</strong> ' . $donnees['nb_Passagers'] . '</p>';
		echo '<p><strong>Correspondance :</strong> ' . $donnees['correspondance'] . '</p>';
		echo '<p><strong>Catégorie :</strong> ' . $donnees['categorie'] . '</p>';
	}

	// Fermeture de la requête et de la connexion à la base de données
	$req->closeCursor();
	$bdd = null;
?>