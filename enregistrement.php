<HTML>
    <HEAD>
        <TITLE>Enregistrement</TITLE>
    </HEAD>
    <BODY>
        <H1>Enregistrement</H1>
        <?php

        $dsn = 'mysql:host=localhost;dbname=tpfinal';

        try {
            $bdd = new PDO($dsn, 'root' , '');
        }
        catch (Exception $e)
        {
            die('Erreur : ' . $e->getMessage());
        }

        $sql = "INSERT INTO `vols`(`id`, `type`, `ville`, `duree`, `horaire`, `avion`, `nb_Passagers`, `correspondance`, `categorie`) VALUES (NULL, :typ, :ville, :duree, :horaire, :avion, :nb_passagers, :correspondance, :categorie);";
        $reponse = $bdd->prepare($sql);

        $reponse -> bindParam ( ':typ' , $typ );
        $reponse -> bindParam ( ':ville' , $ville );
        $reponse -> bindParam ( ':duree' , $duree );
        $reponse -> bindParam ( ':horaire' , $horaire );
        $reponse -> bindParam ( ':avion' , $avion );
        $reponse -> bindParam ( ':nb_passagers' , $nb_passagers );
        $reponse -> bindParam ( ':correspondance' , $correspondance );
        $reponse -> bindParam ( ':categorie' , $categorie );

        $typ = $_POST['type'];
        $ville = $_POST['provdest'];
        $duree = $_POST['duree'];
        $horaire = $_POST['horaire'];
        $avion = $_POST['modele'];
        $nb_passagers = $_POST['nbpass'];
        $correspondance = $_POST['correspondance'];
        $categorie = $_POST['categorie'];

        $res = $reponse -> execute () or die(printr($bdd->errorInfo()));

        if ($res) {
            echo "<h2>Nouvel enregistrement stock&eacute; avec succ&egrave;s dans la table.</h2>" ;
         } else {
            echo "<h2>Une erreur est survenue lors de l'insertion de l'enregistrement dans la table.</h2>" ;
         } ;

        ?>
    </BODY>
</HTML>